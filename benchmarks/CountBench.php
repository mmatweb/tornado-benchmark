<?php

use M6Web\Tornado\EventLoop;
use M6Web\Tornado\Adapter;
use PhpBench\Benchmark\Metadata\Annotations\OutputTimeUnit;
use PhpBench\Benchmark\Metadata\Annotations\ParamProviders;
use PhpBench\Benchmark\Metadata\Annotations\Revs;
use PhpBench\Benchmark\Metadata\Annotations\Sleep;

/**
 * @Revs(1)
 * @OutputTimeUnit("milliseconds")
 */
class CountBench
{
    public function config(): \Generator
    {
        yield ['usleep' => 0,          'countTo' => 10**2, 'divideBY' => 10**1,];
        yield ['usleep' => 100,        'countTo' => 10**2, 'divideBY' => 10**1,];
        yield ['usleep' => 1000,       'countTo' => 10**2, 'divideBY' => 10**1,];
        yield ['usleep' => 10000,      'countTo' => 10**2, 'divideBY' => 10**1,];
//        yield ['usleep' => 100000,     'countTo' => 10**2, 'divideBY' => 10**1,];
//        yield ['usleep' => 1000000,    'countTo' => 10**2, 'divideBY' => 10**1,];
//        yield ['usleep' => 100,        'countTo' => 10**3, 'divideBY' => 10**2,];
//        yield ['usleep' => 1000,       'countTo' => 10**3, 'divideBY' => 10**2,];
//        yield ['usleep' => 10000,      'countTo' => 10**3, 'divideBY' => 10**2,];
//        yield ['usleep' => 100,        'countTo' => 10**4, 'divideBY' => 10**3,];
//        yield ['usleep' => 1000,       'countTo' => 10**4, 'divideBY' => 10**3,];
//        yield ['usleep' => 10000,      'countTo' => 10**4, 'divideBY' => 10**3,];
//        yield ['usleep' => 100,        'countTo' => 10**5, 'divideBY' => 10**4,];
//        yield ['usleep' => 1000,       'countTo' => 10**5, 'divideBY' => 10**4,];
//        yield ['usleep' => 10000,      'countTo' => 10**5, 'divideBY' => 10**4,];
//        yield ['usleep' => 25000,      'countTo' => 10**3, 'divideBY' => 10**2,];
//        yield ['usleep' => 50000,      'countTo' => 10**3, 'divideBY' => 10**2,];
//        yield ['usleep' => 100000,     'countTo' => 10**3, 'divideBY' => 10**2,];
    }
    public function countTo(int $limit, int $usleep)
    {
        $origin = $limit;
        while($limit > 0) {
            $limit--;
        }
        usleep($usleep);

        return $origin;
    }

    public function asyncCountTo(EventLoop $eventLoop, int $limit, int $usleep)
    {
        while($limit > 0) {
            $limit--;
            yield $eventLoop->idle();
        }
        usleep($usleep);
    }

    /**
     * @ParamProviders({"config"})
     * @Sleep(100000)
     */
    public function benchCountOneBatch($config)
    {
        $this->countTo($config['countTo'], $config['usleep']);
    }

    /**
     * @ParamProviders({"config"})
     */
    public function benchCountByBatch($config)
    {
        $i = $config['divideBY'];
        while ($i > 0) {
            $this->countTo($config['countTo'] / $config['divideBY'], $config['usleep']);
            $i--;
        }
    }

//    public function asynchronousCount(EventLoop $eventLoop, int $limit, int $usleep): \Generator
//    {
//        $this->countTo($limit, $usleep);
//        yield $eventLoop->idle();
//    }

    /**
     * @ParamProviders({"config"})
     * @Sleep(100000)
     */
    public function benchCountAsynchronouslyBatch($config)
    {
        $eventLoop = new Adapter\Tornado\EventLoop();
        $promiseStack = [];
        $i = $config['divideBY'];
        while ($i > 0) {
            $promiseStack[] = $eventLoop->async($this->asyncCountTo($eventLoop, $config['countTo'] / $config['divideBY'], $config['usleep'] ));
            $i--;
        }
        $eventLoop->wait($eventLoop->promiseAll(...$promiseStack));
    }

    /**
     * @ParamProviders({"config"})
     * @Sleep(100000)
     */
    public function benchCountByFork($config)
    {
//        $file = __DIR__ . '/export-pid-' . $config['usleep'] . '.txt';
//        if (file_exists($file)) {
//            unlink($file);
//        }
        $stack = [];
        // This loop creates a new fork for each of the items in $tasks.
        $i = $config['divideBY'];
        while ($i > 0) {
            $i--;
            $pid = pcntl_fork();
//            file_put_contents($file, $pid . "\n", FILE_APPEND | LOCK_EX);
            if ($pid === -1) {
//                file_put_contents($file, "fork error\n", FILE_APPEND | LOCK_EX);
                exit("Error forking...\n");
            }

            if ($pid === 0) {
                $stack[] = $this->countTo($config['countTo'] / $config['divideBY'], $config['usleep']);
                exit();
            }
        }

        while(pcntl_waitpid(0, $status) !== -1) {}

        $file = __DIR__ . '/export-pid-' . $config['usleep'] . '.txt';
        if (file_exists($file)) {
            unlink($file);
        }
        foreach ($stack as $value) {
            file_put_contents($file, $value . "\n", FILE_APPEND | LOCK_EX);
        }
    }
}
