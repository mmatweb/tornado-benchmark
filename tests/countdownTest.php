<?php


use M6Web\Tornado\EventLoop;
use M6Web\Tornado\Adapter;
use PHPUnit\Framework\TestCase;

class countdownTest extends TestCase
{
    public function asynchronousCountdown(EventLoop $eventLoop, string $name, int $count): \Generator
    {
        echo "[$name]\tLet me countdown from $count to 0.\n";
        for ($i = $count; $i >= 0; $i--) {
            echo "[$name]\t$i\n";
            // Let the event loop process other jobs before to continue.
            yield $eventLoop->idle();
        }
        echo "[$name] Bye!\n";
        return "[$name] Countdown $count";
    }

    public function testPromise()
    {
        $eventLoop = new Adapter\Tornado\EventLoop();
        $promiseAlice10 = $eventLoop->async($this->asynchronousCountdown($eventLoop, 'Alice', 10));
        $promiseBob4 = $eventLoop->async($this->asynchronousCountdown($eventLoop, 'Bob', 4));
        echo "\nLet's start!\n";
        $result = $eventLoop->wait(
            // We group both promises in one, to run them concurrently.
            $eventLoop->promiseAll($promiseAlice10, $promiseBob4)
        );
        echo "Finished!\n";
    }
}
