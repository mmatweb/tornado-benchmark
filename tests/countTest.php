<?php

use M6Web\Tornado\EventLoop;
use M6Web\Tornado\Adapter;
use PHPUnit\Framework\TestCase;


class countTest extends TestCase
{
    public function countTo(int $limit)
    {
        while($limit > 0) {
            $limit--;
        }
    }

    public function testCountOneBatch()
    {
        $this->countTo(10000000);
    }

    public function testCountLotBatch()
    {
        $i = 1000;
        while ($i > 0) {
            $this->countTo(10000);
            $i--;
        }
    }

    public function asynchronousCount(EventLoop $eventLoop, int $limit): \Generator
    {
        $this->countTo($limit);
        yield $eventLoop->idle();
    }

    public function testCountAsynchronouslyBatch()
    {
        $eventLoop = new Adapter\Tornado\EventLoop();
        $promiseStack = [];
        $i = 1000;
        while ($i > 0) {
            $promiseStack[] = $eventLoop->async($this->asynchronousCount($eventLoop, 10000));
            $i--;
        }
        $eventLoop->wait(
            $eventLoop->promiseAll(...$promiseStack)
        );
    }
}
